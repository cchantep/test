import * as React from 'react';
import MultiSelect from 'react-select';
import { DetailDommageSousNature, Dommage } from '../../../model/resources/chiffrage';

interface SelectorLineProps {
  addLine: (detailName: string) => void
  clearable: boolean
  disabled: boolean
  searchable: boolean
  selectValue: string
  labels: any[]
  prejudice: Dommage
  options: DetailDommageSousNature[]
}

export default class SelectorLine extends React.Component<SelectorLineProps, void> {
  render() {
    const { options, labels, clearable, disabled, selectValue, addLine, searchable } = this.props;
    const listePrejudices = options
      .filter(item => ! labels.find(label => label === item.name))
      .map(item => { return { value: item.name, label: item.label} });

    return (
      <div className="tr tr-sous-nature">
        <div className="td">
          <MultiSelect
            ref="stateSelect"
            options={ listePrejudices }
            simpleValue
            clearable={ clearable }
            name="selected-state"
            disabled={ disabled }
            value={ selectValue }
            onChange={ addLine }
            searchable={ searchable }
            placeholder="Sélectionner une sous-nature…"
            noResultsText="Aucun résultat"
          />
        </div>
        <div className="td"></div>
        <div className="td"></div>
        <div className="td"></div>
      </div>
    )
  }
}
